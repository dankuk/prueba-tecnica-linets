<?php

namespace App\Http\Controllers;

use App\Linets;
use Illuminate\Http\Request;

class LinetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos = Linets::get();
        return view('linets/index', compact('datos'));
    }

    public function agruparModelos(){
        $model = Linets::distinct()->get(['model',"name","price","attribute_set_code","product_type","categories","product_websites","name","description","short_description","weight","product_online","tax_class_name","visibility","price","special_price","special_price_from_date","special_price_to_date","url_key","meta_title","meta_keywords","meta_description","base_image","base_image_label","small_image","small_image_label","thumbnail_image","thumbnail_image_label","swatch_image","swatch_image_label","created_at","updated_at","new_from_date","new_to_date","display_product_options_in","map_price","msrp_price","map_enabled","gift_message_available","custom_design","custom_design_from","custom_design_to","custom_layout_update","page_layout","product_options_container","msrp_display_actual_price_type","country_of_manufacture","additional_attributes","qty","out_of_stock_qty","use_config_min_qty","is_qty_decimal","allow_backorders","use_config_backorders","min_cart_qty","use_config_min_sale_qty","max_cart_qty","use_config_max_sale_qty","is_in_stock","notify_on_stock_below","use_config_notify_stock_qty","manage_stock","use_config_manage_stock","use_config_qty_increments","qty_increments","use_config_enable_qty_inc","enable_qty_increments","is_decimal_divided","website_id","related_skus","related_position","crosssell_skus","crosssell_position","upsell_skus","upsell_position","additional_images","additional_image_labels","hide_from_product_page","custom_options","bundle_price_type","bundle_sku_type","bundle_price_view","bundle_weight_type","bundle_values","bundle_shipment_type","configurable_variation_labels","associated_skus"]);
        $arreglo = [];
        $auxIncremetal = 0;
        foreach($model as $m){
            $producto = Linets::select( 'sku','attribute_color')->where("model","=",$m->model)->get();
            $arreglo[$auxIncremetal]['sku'] = $m->model;
            $arreglo[$auxIncremetal]['name'] = $m->name;
            $arreglo[$auxIncremetal]['price'] = $m->price;

            /*$arreglo[$auxIncremetal]["store_view_code"] = $m->store_view_code;
            $arreglo[$auxIncremetal]["attribute_set_code"] = $m->attribute_set_code;
            $arreglo[$auxIncremetal]["product_type"] = $m->product_type;
            $arreglo[$auxIncremetal]["categories"] = $m->categories;
            $arreglo[$auxIncremetal]["product_websites"] = $m->product_websites;
            $arreglo[$auxIncremetal]["description"] = $m->description;
            $arreglo[$auxIncremetal]["short_description"] = $m->short_description;
            $arreglo[$auxIncremetal]["weight"] = $m->weight;
            $arreglo[$auxIncremetal]["product_online"] = $m->product_online;
            $arreglo[$auxIncremetal]["tax_class_name"] = $m->tax_class_name;
            $arreglo[$auxIncremetal]["visibility"] = $m->visibility;
            $arreglo[$auxIncremetal]["special_price"] = $m->special_price;
            $arreglo[$auxIncremetal]["special_price_from_date"] = $m->special_price_from_date;
            $arreglo[$auxIncremetal]["special_price_to_date"] = $m->special_price_to_date;
            $arreglo[$auxIncremetal]["url_key"] = $m->url_key;
            $arreglo[$auxIncremetal]["meta_title"] = $m->meta_title;
            $arreglo[$auxIncremetal]["meta_keywords"] = $m->meta_keywords;
            $arreglo[$auxIncremetal]["meta_description"] = $m->meta_description;
            $arreglo[$auxIncremetal]["base_image"] = $m->base_image;
            $arreglo[$auxIncremetal]["base_image_label"] = $m->base_image_label;
            $arreglo[$auxIncremetal]["small_image"] = $m->small_image;
            $arreglo[$auxIncremetal]["small_image_label"] = $m->small_image_label;
            $arreglo[$auxIncremetal]["thumbnail_image"] = $m->thumbnail_image;
            $arreglo[$auxIncremetal]["thumbnail_image_label"] = $m->thumbnail_image_label;
            $arreglo[$auxIncremetal]["swatch_image"] = $m->swatch_image;
            $arreglo[$auxIncremetal]["swatch_image_label"] = $m->swatch_image_label;
            $arreglo[$auxIncremetal]["created_at"] = $m->created_at;
            $arreglo[$auxIncremetal]["updated_at"] = $m->updated_at;
            $arreglo[$auxIncremetal]["new_from_date"] = $m->new_from_date;
            $arreglo[$auxIncremetal]["new_to_date"] = $m->new_to_date;
            $arreglo[$auxIncremetal]["display_product_options_in"] = $m->display_product_options_in;
            $arreglo[$auxIncremetal]["map_price"] = $m->map_price;
            $arreglo[$auxIncremetal]["msrp_price"] = $m->msrp_price;
            $arreglo[$auxIncremetal]["map_enabled"] = $m->map_enabled;
            $arreglo[$auxIncremetal]["gift_message_available"] = $m->gift_message_available;
            $arreglo[$auxIncremetal]["custom_design"] = $m->custom_design;
            $arreglo[$auxIncremetal]["custom_design_from"] = $m->custom_design_from;
            $arreglo[$auxIncremetal]["custom_design_to"] = $m->custom_design_to;
            $arreglo[$auxIncremetal]["custom_layout_update"] = $m->custom_layout_update;
            $arreglo[$auxIncremetal]["page_layout"] = $m->page_layout;
            $arreglo[$auxIncremetal]["product_options_container"] = $m->product_options_container;
            $arreglo[$auxIncremetal]["msrp_display_actual_price_type"] = $m->msrp_display_actual_price_type;
            $arreglo[$auxIncremetal]["country_of_manufacture"] = $m->country_of_manufacture;
            $arreglo[$auxIncremetal]["additional_attributes"] = $m->additional_attributes;
            $arreglo[$auxIncremetal]["qty"] = $m->qty;
            $arreglo[$auxIncremetal]["out_of_stock_qty"] = $m->out_of_stock_qty;
            $arreglo[$auxIncremetal]["use_config_min_qty"] = $m->use_config_min_qty;
            $arreglo[$auxIncremetal]["is_qty_decimal"] = $m->is_qty_decimal;
            $arreglo[$auxIncremetal]["allow_backorders"] = $m->allow_backorders;
            $arreglo[$auxIncremetal]["use_config_backorders"] = $m->use_config_backorders;
            $arreglo[$auxIncremetal]["min_cart_qty"] = $m->min_cart_qty;
            $arreglo[$auxIncremetal]["use_config_min_sale_qty"] = $m->use_config_min_sale_qty;
            $arreglo[$auxIncremetal]["max_cart_qty"] = $m->max_cart_qty;
            $arreglo[$auxIncremetal]["use_config_max_sale_qty"] = $m->use_config_max_sale_qty;
            $arreglo[$auxIncremetal]["is_in_stock"] = $m->is_in_stock;
            $arreglo[$auxIncremetal]["notify_on_stock_below"] = $m->notify_on_stock_below;
            $arreglo[$auxIncremetal]["use_config_notify_stock_qty"] = $m->use_config_notify_stock_qty;
            $arreglo[$auxIncremetal]["manage_stock"] = $m->manage_stock;
            $arreglo[$auxIncremetal]["use_config_manage_stock"] = $m->use_config_manage_stock;
            $arreglo[$auxIncremetal]["use_config_qty_increments"] = $m->use_config_qty_increments;
            $arreglo[$auxIncremetal]["qty_increments"] = $m->qty_increments;
            $arreglo[$auxIncremetal]["use_config_enable_qty_inc"] = $m->use_config_enable_qty_inc;
            $arreglo[$auxIncremetal]["enable_qty_increments"] = $m->enable_qty_increments;
            $arreglo[$auxIncremetal]["is_decimal_divided"] = $m->is_decimal_divided;
            $arreglo[$auxIncremetal]["website_id"] = $m->website_id;
            $arreglo[$auxIncremetal]["related_skus"] = $m->related_skus;
            $arreglo[$auxIncremetal]["related_position"] = $m->related_position;
            $arreglo[$auxIncremetal]["crosssell_skus"] = $m->crosssell_skus;
            $arreglo[$auxIncremetal]["crosssell_position"] = $m->crosssell_position;
            $arreglo[$auxIncremetal]["upsell_skus"] = $m->upsell_skus;
            $arreglo[$auxIncremetal]["upsell_position"] = $m->upsell_position;
            $arreglo[$auxIncremetal]["additional_images"] = $m->additional_images;
            $arreglo[$auxIncremetal]["additional_image_labels"] = $m->additional_image_labels;
            $arreglo[$auxIncremetal]["hide_from_product_page"] = $m->hide_from_product_page;
            $arreglo[$auxIncremetal]["custom_options"] = $m->custom_options;
            $arreglo[$auxIncremetal]["bundle_price_type"] = $m->bundle_price_type;
            $arreglo[$auxIncremetal]["bundle_sku_type"] = $m->bundle_sku_type;
            $arreglo[$auxIncremetal]["bundle_price_view"] = $m->bundle_price_view;
            $arreglo[$auxIncremetal]["bundle_weight_type"] = $m->bundle_weight_type;
            $arreglo[$auxIncremetal]["bundle_values"] = $m->bundle_values;
            $arreglo[$auxIncremetal]["bundle_shipment_type"] = $m->bundle_shipment_type;
            $arreglo[$auxIncremetal]["configurable_variations"] = $m->configurable_variations;
            $arreglo[$auxIncremetal]["configurable_variation_labels"] = $m->configurable_variation_labels;
            $arreglo[$auxIncremetal]["associated_skus"] = $m->associated_skus;*/


            $configurations_variatons = "";
            $cantElementos = count($producto);
            $auxElemeto = 1;
            foreach($producto as $p){
                $configurations_variatons .= "sku=".$p->sku.",color=".$p->attribute_color;
                if($auxElemeto < $cantElementos){
                    $configurations_variatons .= "|";
                    $auxElemeto++;
                }
            }
            $arreglo[$auxIncremetal]['configurations_variatons'] = $configurations_variatons;
            $auxIncremetal++;
        }
        return $this->exportToCsv($arreglo);
    }




    public function exportToCsv($datos)
    {
        $fileName = 'datos.csv';

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        //$columns = array('sku','store_view_code','attribute_set_code','product_type','categories','product_websites','name','description','short_description','weight','product_online','tax_class_name','visibility','price','special_price','special_price_from_date','special_price_to_date','url_key','meta_title','meta_keywords','meta_description','base_image','base_image_label','small_image','small_image_label','thumbnail_image','thumbnail_image_label','swatch_image','swatch_image_label','created_at','updated_at','new_from_date','new_to_date','display_product_options_in','map_price','msrp_price','map_enabled','gift_message_available','custom_design','custom_design_from','custom_design_to','custom_layout_update','page_layout','product_options_container','msrp_display_actual_price_type','country_of_manufacture','additional_attributes','qty','out_of_stock_qty','use_config_min_qty','is_qty_decimal','allow_backorders','use_config_backorders','min_cart_qty','use_config_min_sale_qty','max_cart_qty','use_config_max_sale_qty','is_in_stock','notify_on_stock_below','use_config_notify_stock_qty','manage_stock','use_config_manage_stock','use_config_qty_increments','qty_increments','use_config_enable_qty_inc','enable_qty_increments','is_decimal_divided','website_id','related_skus','related_position','crosssell_skus','crosssell_position','upsell_skus','upsell_position','additional_images','additional_image_labels','hide_from_product_page','custom_options','bundle_price_type','bundle_sku_type','bundle_price_view','bundle_weight_type','bundle_values','bundle_shipment_type','configurable_variations','configurable_variation_labels','associated_skus');
        $columns = array('sku','name','price','configurations_variatons');

        $callback = function() use($datos, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns, ";");

            foreach ($datos as $dato) {

                $row["sku"]=utf8_decode($dato['sku']);
                $row["name"]=utf8_decode($dato["name"]);
                $row["price"]=utf8_decode($dato["price"]);
                $row["configurations_variatons"]=utf8_decode($dato["configurations_variatons"]);


                /*
                $row["sku"]=utf8_decode($dato->sku);
                $row["store_view_code"]=utf8_decode($dato->store_view_code);
                $row["attribute_set_code"]=utf8_decode($dato->attribute_set_code);
                $row["product_type"]=utf8_decode($dato->product_type);
                $row["categories"]=utf8_decode($dato->categories);
                $row["product_websites"]=utf8_decode($dato->product_websites);
                $row["name"]=utf8_decode($dato->name);
                $row["description"]=utf8_decode($dato->description);
                $row["short_description"]=utf8_decode($dato->short_description);
                $row["weight"]=utf8_decode($dato->weight);
                $row["product_online"]=utf8_decode($dato->product_online);
                $row["tax_class_name"]=utf8_decode($dato->tax_class_name);
                $row["visibility"]=utf8_decode($dato->visibility);
                $row["price"]=utf8_decode($dato->price);
                $row["special_price"]=utf8_decode($dato->special_price);
                $row["special_price_from_date"]=utf8_decode($dato->special_price_from_date);
                $row["special_price_to_date"]=utf8_decode($dato->special_price_to_date);
                $row["url_key"]=utf8_decode($dato->url_key);
                $row["meta_title"]=utf8_decode($dato->meta_title);
                $row["meta_keywords"]=utf8_decode($dato->meta_keywords);
                $row["meta_description"]=utf8_decode($dato->meta_description);
                $row["base_image"]=utf8_decode($dato->base_image);
                $row["base_image_label"]=utf8_decode($dato->base_image_label);
                $row["small_image"]=utf8_decode($dato->small_image);
                $row["small_image_label"]=utf8_decode($dato->small_image_label);
                $row["thumbnail_image"]=utf8_decode($dato->thumbnail_image);
                $row["thumbnail_image_label"]=utf8_decode($dato->thumbnail_image_label);
                $row["swatch_image"]=utf8_decode($dato->swatch_image);
                $row["swatch_image_label"]=utf8_decode($dato->swatch_image_label);
                $row["created_at"]=utf8_decode($dato->created_at);
                $row["updated_at"]=utf8_decode($dato->updated_at);
                $row["new_from_date"]=utf8_decode($dato->new_from_date);
                $row["new_to_date"]=utf8_decode($dato->new_to_date);
                $row["display_product_options_in"]=utf8_decode($dato->display_product_options_in);
                $row["map_price"]=utf8_decode($dato->map_price);
                $row["msrp_price"]=utf8_decode($dato->msrp_price);
                $row["map_enabled"]=utf8_decode($dato->map_enabled);
                $row["gift_message_available"]=utf8_decode($dato->gift_message_available);
                $row["custom_design"]=utf8_decode($dato->custom_design);
                $row["custom_design_from"]=utf8_decode($dato->custom_design_from);
                $row["custom_design_to"]=utf8_decode($dato->custom_design_to);
                $row["custom_layout_update"]=utf8_decode($dato->custom_layout_update);
                $row["page_layout"]=utf8_decode($dato->page_layout);
                $row["product_options_container"]=utf8_decode($dato->product_options_container);
                $row["msrp_display_actual_price_type"]=utf8_decode($dato->msrp_display_actual_price_type);
                $row["country_of_manufacture"]=utf8_decode($dato->country_of_manufacture);
                $row["additional_attributes"]=utf8_decode($dato->additional_attributes);
                $row["qty"]=utf8_decode($dato->qty);
                $row["out_of_stock_qty"]=utf8_decode($dato->out_of_stock_qty);
                $row["use_config_min_qty"]=utf8_decode($dato->use_config_min_qty);
                $row["is_qty_decimal"]=utf8_decode($dato->is_qty_decimal);
                $row["allow_backorders"]=utf8_decode($dato->allow_backorders);
                $row["use_config_backorders"]=utf8_decode($dato->use_config_backorders);
                $row["min_cart_qty"]=utf8_decode($dato->min_cart_qty);
                $row["use_config_min_sale_qty"]=utf8_decode($dato->use_config_min_sale_qty);
                $row["max_cart_qty"]=utf8_decode($dato->max_cart_qty);
                $row["use_config_max_sale_qty"]=utf8_decode($dato->use_config_max_sale_qty);
                $row["is_in_stock"]=utf8_decode($dato->is_in_stock);
                $row["notify_on_stock_below"]=utf8_decode($dato->notify_on_stock_below);
                $row["use_config_notify_stock_qty"]=utf8_decode($dato->use_config_notify_stock_qty);
                $row["manage_stock"]=utf8_decode($dato->manage_stock);
                $row["use_config_manage_stock"]=utf8_decode($dato->use_config_manage_stock);
                $row["use_config_qty_increments"]=utf8_decode($dato->use_config_qty_increments);
                $row["qty_increments"]=utf8_decode($dato->qty_increments);
                $row["use_config_enable_qty_inc"]=utf8_decode($dato->use_config_enable_qty_inc);
                $row["enable_qty_increments"]=utf8_decode($dato->enable_qty_increments);
                $row["is_decimal_divided"]=utf8_decode($dato->is_decimal_divided);
                $row["website_id"]=utf8_decode($dato->website_id);
                $row["related_skus"]=utf8_decode($dato->related_skus);
                $row["related_position"]=utf8_decode($dato->related_position);
                $row["crosssell_skus"]=utf8_decode($dato->crosssell_skus);
                $row["crosssell_position"]=utf8_decode($dato->crosssell_position);
                $row["upsell_skus"]=utf8_decode($dato->upsell_skus);
                $row["upsell_position"]=utf8_decode($dato->upsell_position);
                $row["additional_images"]=utf8_decode($dato->additional_images);
                $row["additional_image_labels"]=utf8_decode($dato->additional_image_labels);
                $row["hide_from_product_page"]=utf8_decode($dato->hide_from_product_page);
                $row["custom_options"]=utf8_decode($dato->custom_options);
                $row["bundle_price_type"]=utf8_decode($dato->bundle_price_type);
                $row["bundle_sku_type"]=utf8_decode($dato->bundle_sku_type);
                $row["bundle_price_view"]=utf8_decode($dato->bundle_price_view);
                $row["bundle_weight_type"]=utf8_decode($dato->bundle_weight_type);
                $row["bundle_values"]=utf8_decode($dato->bundle_values);
                $row["bundle_shipment_type"]=utf8_decode($dato->bundle_shipment_type);
                $row["configurable_variations"]=utf8_decode($dato->configurable_variations);
                $row["configurable_variation_labels"]=utf8_decode($dato->configurable_variation_labels);
                $row["associated_skus"]=utf8_decode($dato->associated_skus);
                fputcsv($file, array($row["sku"],$row["store_view_code"],$row["attribute_set_code"],$row["product_type"],$row["categories"],$row["product_websites"],$row["name"],$row["description"],$row["short_description"],$row["weight"],$row["product_online"],$row["tax_class_name"],$row["visibility"],$row["price"],$row["special_price"],$row["special_price_from_date"],$row["special_price_to_date"],$row["url_key"],$row["meta_title"],$row["meta_keywords"],$row["meta_description"],$row["base_image"],$row["base_image_label"],$row["small_image"],$row["small_image_label"],$row["thumbnail_image"],$row["thumbnail_image_label"],$row["swatch_image"],$row["swatch_image_label"],$row["created_at"],$row["updated_at"],$row["new_from_date"],$row["new_to_date"],$row["display_product_options_in"],$row["map_price"],$row["msrp_price"],$row["map_enabled"],$row["gift_message_available"],$row["custom_design"],$row["custom_design_from"],$row["custom_design_to"],$row["custom_layout_update"],$row["page_layout"],$row["product_options_container"],$row["msrp_display_actual_price_type"],$row["country_of_manufacture"],$row["additional_attributes"],$row["qty"],$row["out_of_stock_qty"],$row["use_config_min_qty"],$row["is_qty_decimal"],$row["allow_backorders"],$row["use_config_backorders"],$row["min_cart_qty"],$row["use_config_min_sale_qty"],$row["max_cart_qty"],$row["use_config_max_sale_qty"],$row["is_in_stock"],$row["notify_on_stock_below"],$row["use_config_notify_stock_qty"],$row["manage_stock"],$row["use_config_manage_stock"],$row["use_config_qty_increments"],$row["qty_increments"],$row["use_config_enable_qty_inc"],$row["enable_qty_increments"],$row["is_decimal_divided"],$row["website_id"],$row["related_skus"],$row["related_position"],$row["crosssell_skus"],$row["crosssell_position"],$row["upsell_skus"],$row["upsell_position"],$row["additional_images"],$row["additional_image_labels"],$row["hide_from_product_page"],$row["custom_options"],$row["bundle_price_type"],$row["bundle_sku_type"],$row["bundle_price_view"],$row["bundle_weight_type"],$row["bundle_values"],$row["bundle_shipment_type"],$row["configurable_variations"],$row["configurable_variation_labels"],$row["associated_skus"]), ";");*/

                fputcsv($file, array($row["sku"],$row["name"],$row["price"],$row["configurations_variatons"]), ";");
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('linets/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $dato = new Linets();
        $dato->model = $request->model;
        $dato->sku = $request->sku;
        $dato->name = $request->name;
        $dato->price = $request->price;
        $dato->attribute_color = $request->attribute_color;
        $dato->save();
        return redirect()->route('index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertRest(Request $request)
    {
        $dato = new Linets();
        $dato->model = $request->model;
        $dato->sku = $request->sku;
        $dato->name = $request->name;
        $dato->price = $request->price;
        $dato->attribute_color = $request->attribute_color;
        $dato->save();
        return "Registro OK!!!";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Linets  $linets
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //dd($sku);

        //$dato = Linets::findOrFail($request->sku);
        $dato = Linets::where('sku','=',$request->sku)->get();
        return $dato;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Linets  $linets
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Linets  $linets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $dato = Linets::findOrFail($request->sku);
        $dato->model = $request->model;
        $dato->sku = $request->sku;
        $dato->name = $request->name;
        $dato->price = $request->price;
        $dato->attribute_color = $request->attribute_color;

        $dato->save();

        return $dato;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Linets  $linets
     * @return \Illuminate\Http\Response
     */
    public function destroy($sku)
    {
        //dd($sku);
        $dato = Linets::where("sku","=",$sku)->delete();
        return redirect()->route('index');
    }
}
