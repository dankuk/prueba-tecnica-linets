<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Index: listado de registros en BDD */
Route::get('/', 'LinetsController@index' )->name("index");

/*  ruta para exportacion a CSV */
Route::get('csv-export', 'LinetsController@exportToCsv');



Route::put('actualizar', 'LinetsController@update');
Route::post('insertar', 'LinetsController@insertRest');
Route::delete('borrar/{sku}', 'LinetsController@destroy')->name("eliminar");
Route::get('buscar', 'LinetsController@show');
Route::get('crear', 'LinetsController@create');
Route::post('store', 'LinetsController@store')->name("store");
Route::get('agruparModelos', 'LinetsController@agruparModelos')->name('csv-export');
