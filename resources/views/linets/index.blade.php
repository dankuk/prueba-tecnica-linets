@extends('header')

<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <a href="/crear" id="export" class="btn btn-primary btn-sm">Agregar registro</a>
                <a href="{{ route('csv-export') }}" id="export" class="btn btn-info btn-sm">Exportar a CSV</a>
            </div>

            <div class="col-12">
                <table id="listado" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>model</th>
                            <th>sku</th>
                            <th>price</th>
                            <th>name</th>
                            <th>attribute color</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($datos as $item)
                            <tr>
                                <td>
                                    {{ $item->model }}
                                </td>
                                <td>
                                    {{ $item->sku }}
                                </td>
                                <td>
                                    {{ $item->price }}
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->attribute_color }}
                                </td>
                                <td>


                                    <form action="{{ route('eliminar', ['sku' => $item->sku]) }}" method="post">
                                        <input class="btn btn-danger" type="submit" value="Eliminar" />
                                        <input type="hidden" name="_method" value="delete" />
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                </td>
                            @empty
                                <td colspan="5"> no hay registros para mostrar </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @extends('scripts')
</body>

</html>
