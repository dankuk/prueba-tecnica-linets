@extends('header')
<div class="container">
    <div class="row">
        <form id="formFormularios" role="form" action="{{ route('store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group col-12 mx-sm-12">
                <label for="inputUser" class="sr-only">Model</label>
                <input type="text" class="form-control" id="model" name="model" placeholder="Model">
            </div>
            <div class="form-group col-12 mx-sm-12">
                <label for="inputPass" class="sr-only">SKu</label>
                <input type="text" class="form-control" id="sku" name="sku" placeholder="Sku">
            </div>
            <div class="form-group col-12 mx-sm-12">
                <label for="inputPass" class="sr-only">Price</label>
                <input type="text" class="form-control" id="price" name="price" placeholder="Price">
            </div>
            <div class="form-group col-12 mx-sm-12">
                <label for="inputPass" class="sr-only">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            </div>
            <div class="form-group col-12 mx-sm-12">
                <label for="inputPass" class="sr-only">Attribute Color</label>
                <input type="text" class="form-control" id="attribute_color" name="attribute_color" placeholder="Attribute Color">
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
         </form>
    </div>
</div>

 @extends('scripts')
