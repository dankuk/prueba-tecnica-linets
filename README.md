# Prueba Técnica Linets

El desafío consiste en tomar información de una base de datos en sqlite3 y llevarlo a un archivo
plano csv utilizando Laravel. (puede re-escribir el modelo en otro motor si lo considera necesario)
El modelo tiene varios registros con la siguiente estructura
model|sku|...|attribute_color|...
Donde los campos más relevantes son:
● Model: es el modelo o elemento agrupador
● Sku: identificador del registro
● Price: precio
● Name: nombre del producto
● attribute_color: color del registro
El csv resultante debe agrupar todos los modelos y generar un fichero de la siguiente forma
sku(model)|..|name|price|...|configurable_variatons
Donde:
SKU: debe colocar el modelo agrupado (campo model)
name: nombre del producto
price: precio
configurable_variatons: Debe concatenar un string de la siguiente forma
sku=1234,color=red|sku=455,color=blue

## Instalación Docker🚀

Acceder a la web oficial pinchando [aquí][link-docker], para comenzar es necesario estar registrado.

Se debe instalar  en su ultima versión 

## Laradock
Laradock es un completo entorno de desarrollo PHP para Docker.

Incluye imágenes Docker preempaquetadas, todas preconfiguradas para proporcionar un fantástico entorno de desarrollo PHP.

Desde la web oficial de laradock puedes consultar todos los servicios de los que dispones:

- Bases de datos: MySQL – MariaDB – Percona – MongoDB – Neo4j – RethinkDB – MSSQL – PostgreSQL –
- Postgres-PostGIS .
- Gestores de bases de datos:
- PhpMyAdmin – Adminer – PgAdmin
- Motores de cache:
- Redis – Memcached – Aerospike
- Servidores PHP:
- NGINX – Apache2 – Caddy
- Compiladores PHP:
- PHP FPM – HHVM
- Cola de mensajes:
- Beanstalkd – RabbitMQ – PHP Worker
- Gestor de colas:
- Beanstalkd Console – RabbitMQ Console
- Herramientas variadas:
-  Mailu – HAProxy – Certbot – Blackfire – Selenium – Jenkins – ElasticSearch – Kibana – Grafana – Gitlab – Mailhog – MailDev – Minio – Varnish – Swoole – NetData – Portainer – Laravel Echo – Phalcon…
- Workspace:
- PHP CLI – Composer – Git – Linuxbrew – Node – V8JS – Gulp – SQLite – xDebug – Envoy – Deployer – Vim – Yarn – SOAP – Drush – WP-CLI…

#### Instalacion de Laradock

Clonar repositorio de LaraDock

`git clone https://github.com/Laradock/laradock.git`

###### Configurar Laradock

Creamos el archivo .env y lo editamos con la finalidad de que Laradock reconozca nuestro proyecto de Laravel buscando la variable DOCKER_HOST_IP y colocando 127.0.0.1

    cd laradock
    cp .env-example .env
    nano .env
	
![](https://miro.medium.com/max/700/1*ThU2rRr6pcWjvh1-L07jZg.png)

#### Arrancar contenedores

`docker-compose up -d nginx mysql phpmyadmin redis workspace `

Si es la primera vez que se ejecuta este comando, el proceso demorará unos minutos, ya que los servicios indicados deben ser descargados. Cuando el proceso haya finalizado, los contenedores estaran ejecutandose. 

Podemos ver los contenedores que hay corriendo utilizando el comando:

`docker-compose ps`


### El contenedor workspace

Para acceder a la terminal del workspace vamos a ejecutar el programa bash del servicio workspace:

`docker-compose exec workspace bash`

Ahora en el propt se verá como una terminal linux y ubicado en el directorio /var/www En este directorio es donde debemos ubicar el código PHP. Por defecto será el directorio de nivel superior al de laradock.

`root@53126cc84031:/var/www#`


#### Clonar repositorio del proyecto *Prueba Tecnica Linets*

`$ git clone https://gitlab.com/dankuk/prueba-tecnica-linets.git`

Ingresamos a la carpeta  **prueba-tecnica-linets**

    $ cd prueba-tecnica-linets

Cramos una copia de .env-example a .env

    $ cp .env.example .env

Editar .env y reemplazar 

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
por esto:

```bash
DB_CONNECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=/var/www/storage/bdd/data.db
DB_USERNAME=root
DB_PASSWORD=
```


#### Configurar el contenedor Nginx para que apunte hacia nuestro proyecto.

Editamos de nuevo el archivo .env de laradock y colocamos el nombre de la carpeta del proyecto en la variable **APP_CODE_PATH_HOST**

    APP_CODE_PATH_HOST = ../prueba-tecnica-linets

![](https://miro.medium.com/max/613/1*mVmsi-mUmRJDrTFW09YcrA.png)

#### Reiniciamos los contenedores.

    docker-compose down
    docker-compose up -d nginx mysql
	
Volvemos a entrar en el Bash del contenedor con el comando:

    docker-compose exec workspace bash
	

al revisar con el comando ls, podremos ver que ahora la ruta /var/www/ corresponde a las fuentes del proyecto **Prueba Técnica Linets**

Procedemos a ejecutar **composer install** para instalar las dependencias faltantes en el ambiente

    $ composer install

Luego ejecutamos 

    $ php artisan key:generate

## NOTA: 
Si presenta inconveniente de escritura en archivos log o session, recuerde revisar los permisos sobre las carpetas en la ruta **/var/www/storage/**, puede cambiar los permisos de manera recursiva con el comando:

    $ chmod -R 755 /var/www/storage
    
Se debe establecer un permiso especial sobre el archivo **data.bd**, para poder leer y escribir en el, pensado solo como un ambiente de laboratorio, no se recomienda usar esta estructura en ambientes de producción, se adjunta en la carpeta **bdd_query** el script para crear una tabla con los campos y estructuras necesarios en caso de requerirlo. 

Para poder ejecutarse como ambiente local, se debe ejecutar el comando:

    chmod -R 777 /var/www/storage/bdd/

## Despliegue 📦

Si todo queda configuado correctamente podra ver el sitio en http://localhost

## Construido con 🛠️

* [Laravel](https://laravel.com/) - El framework web usado
* [Laragon](https://laragon.org/) - Suite de desarrollo PHP
* [Docker](https://www.docker.com/) - Manejador de contenedores
* [Composer](https://getcomposer.org/download/) - Manejador de dependencias

## Versionado 📌

Usamos [Gitlab](https://gitlab.com/) para el versionado. Para todas las versiones disponibles.

## Autor ✒️

* **Daniel Canales Rivas** - *Trabajo Inicial* - [Daniel Canales](https://github.com/dankuk)




[link-docker]: https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe "link"
